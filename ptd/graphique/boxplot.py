from ptd.graphique.graphique import Graphique
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy
import matplotlib.pyplot as plt
import numpy as np


class Boxplot(Graphique):
    """ Création d'une boîte à moustaches 

    Attributes
    ----------
    - variables : list 
        Liste des variables à représenter sous forme de boîte à moustaches 

    Parameters 
    ----------
    - variables : list 
        Liste des variables à représenter sous forme de boîte à moustaches 
    """

    def __init__(self, variables):
        """ Constructeur 

        Parameters 
        ----------
        variables : list 
            Liste des variables à représenter sous forme de boîte à moustaches 
        """
        self.__variables = variables

    def appliquer(self, data):
        """ Affiche les boîte à moustaches de la distribution des variables 

        Parameters
        ----------
        data : Table 
            Jeu de données sur lequel récupérer les variables à représenter sous forme de boîte à moustaches 

        Returns
        -------
        Table 
            Jeu de données initial 
        """
        # On récupère les valeurs des variables à représenter
        D = []
        for i in range(len(self.__variables)):
            # Si l'une des variables renseignée n'est pas dans l'en-tête de la table
            if self.__variables[i] not in data.header():
                raise Exception(
                    'La représentation graphique n\'a pas pu aboutir puisque l\'une des variables n\'est pas présente dans le jeu de données.')
            col = OutilsStats(data.header()).selectionCol(
                data.body(), self.__variables[i])
            col = OutilsStats(col)
            col.nettoyage()
            D.append(col.data())
        # On trouve les indices des boxplot
        compteur = 0
        positions = []
        for i in range(len(D)):
            compteur += 2
            positions.append(compteur)
        # Implémentation des boxplot
        fig, ax = plt.subplots()
        VP = ax.boxplot(D, positions=positions, widths=1.5, patch_artist=True,
                        showmeans=False, showfliers=False,
                        medianprops={"color": "white", "linewidth": 0.5},
                        boxprops={"facecolor": "C0", "edgecolor": "white",
                                  "linewidth": 0.5},
                        whiskerprops={"color": "C0", "linewidth": 1.5},
                        capprops={"color": "C0", "linewidth": 1.5})
        # On cherche la valeur maximale prise par les variables à représenter
        maximum = 0
        for i in range(len(D)):
            if max(D[i]) > maximum:
                maximum = max(D[i])
        ax.set(xlim=(0, int(positions[-1])+2), ylim=(0, maximum+10))
        plt.show()
        return data


if __name__ == '__main__':
    a = Table(['Date', 'Departement', 'Temperature', 'Densite', 'Consommation gaz'], [['12/05/2022', 'Charente-Maritime',
              17, 11, 100], ['12/05/2022', 'Deux-Sèvres', 14, 100, 400], ['12/05/2022', 'Viennes', 10, 6, 200]])
    t = Boxplot(['Temperature', 'Densite'])
    t.appliquer(a)
