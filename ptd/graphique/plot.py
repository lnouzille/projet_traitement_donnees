from ptd.graphique.graphique import Graphique
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy
import matplotlib.pyplot as plt
import numpy as np


class Plot(Graphique):
    """ Création d'un nuage de points 

    Attributes 
    ----------
    - x : str 
        Nom de la variable à représenter en abscisse 
    - y : str 
        Nom de la variable à représenter en ordonnée 

    Parameters
    ----------
    - x : str 
        Nom de la variable à représenter en abscisse 
    - y : str 
        Nom de la variable à représenter en ordonnée
    """

    def __init__(self, x, y):
        """ Constructeur 

        Parameters
        ----------
        x : str 
            Nom de la variable à représenter en abscisse 
        y : str 
            Nom de la variable à représenter en ordonnée
        """
        self.__x = x
        self.__y = y

    def appliquer(self, data):
        """ Affiche le nuage de points avec x en abscisse et y en ordonnée

        Parameters
        ----------
        data : Table 
            Jeu de données sur lequel récupérer les variables à projeter

        Returns
        -------
        Table 
            Jeu de données inital 
        """
        # Si l'une des variables renseignée n'est pas dans l'en-tête de la table
        if (self.__x not in data.header()) or (self.__y not in data.header()):
            raise Exception(
                'La représentation graphique n\'a pas pu aboutir puisque l\'une des variables n\'est pas présente dans le jeu de données.')
        # On récupère les valeurs des variables à représenter et on les convertit en nombre flottant
        x = OutilsStats(data.header()).selectionCol(data.body(), self.__x)
        y = OutilsStats(data.header()).selectionCol(data.body(), self.__y)
        x = OutilsStats(x)
        x.convertirFloat()
        x = x.data()
        y = OutilsStats(y)
        y.convertirFloat()
        y = y.data()
        # On supprime les valeurs manquantes dans les deux variables lorsqu'il y en a au moins une dans l'une des deux variables
        i = 0
        while ('mq' in x) or ('mq' in y) or (' ' in x) or (' ' in y):
            if type(x[i]) is str or type(y[i]) is str:
                del x[i]
                del y[i]
            else:
                i += 1
        if not (len(x) or len(y)):
            raise Exception(
                'Aucune donnée n\'est présente à la fois dans x et dans y')
        # Size and color
        sizes = [30 for i in range(len(x))]
        colors = [40 for i in range(len(x))]
        # Plot
        fig, ax = plt.subplots()
        ax.scatter(x, y, s=sizes, c=colors)
        ax.set(xlim=(min(x)-10, max(x)+10), ylim=(min(y)-100, max(y)+100))
        # Donner un nom aux axes
        ax.set_xlabel(self.__x)
        ax.set_ylabel(self.__y)
        plt.show()
        return data


if __name__ == '__main__':
    a = Table(['Date', 'Departement', 'Temperature', 'Densite', 'Consommation gaz'], [['12/05/2022', 'Charente-Maritime',
              'mq', 11, 100], ['12/05/2022', 'Deux-Sèvres', 14, 100, 400], ['12/05/2022', 'Viennes', 10, 6, 200]])
    t = Plot('Temperature', 'Densite')
    t.appliquer(a)
