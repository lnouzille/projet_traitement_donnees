from abc import ABC, abstractmethod


class Graphique(ABC):
    """ Effectue une représentation graphique """

    @abstractmethod
    def appliquer(self, data):
        """ Création d'un graphique """
        pass
