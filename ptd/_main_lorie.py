from ptd.importation.importcsv import ImportCsv
from ptd.importation.importjson import ImportJson
from ptd.transformation.agregationspatiale import AgregationSpatiale
from ptd.transformation.centrage import Centrage
from ptd.transformation.fenetrage import Fenetrage
from ptd.transformation.jointure import Jointure
from ptd.transformation.moyenneglissante import MoyenneGlissante
from ptd.transformation.normalisation import Normalisation
from ptd.transformation.selectionvariables import SelectionVariables
from ptd.exportation.exportcsv import ExportCsv
from ptd.graphique.plot import Plot
from ptd.graphique.boxplot import Boxplot
from ptd.modele.pipeline import Pipeline
from ptd.modele.table import Table

# Test : Moyenne glissante
tab2_json = Pipeline([ImportJson('./donnees/data/2013-01.json.gz'),
                      SelectionVariables(
                          ['date_heure', 'region', 'consommation_brute_totale', 'consommation_brute_gaz_totale']),
                      MoyenneGlissante(['consommation_brute_totale'], 10),
                      Normalisation(['consommation_brute_gaz_totale']),
                      ExportCsv('mainlorie')])
tab2 = tab2_json.appliquer()