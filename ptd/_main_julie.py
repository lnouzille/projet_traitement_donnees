from ptd.importation.importcsv import ImportCsv
from ptd.importation.importjson import ImportJson
from ptd.transformation.agregationspatiale import AgregationSpatiale
from ptd.transformation.centrage import Centrage
from ptd.transformation.fenetrage import Fenetrage
from ptd.transformation.jointure import Jointure
from ptd.transformation.moyenneglissante import MoyenneGlissante
from ptd.transformation.normalisation import Normalisation
from ptd.transformation.selectionvariables import SelectionVariables
from ptd.exportation.exportcsv import ExportCsv
from ptd.graphique.plot import Plot
from ptd.graphique.boxplot import Boxplot
from ptd.modele.pipeline import Pipeline
from ptd.modele.table import Table

# Test : Relation entre vent et temperature
reg = Pipeline([ImportCsv('./donnees/data/postesSynopAvecRegions.csv')])
reg = reg.appliquer()
tab1_csv = Pipeline([ImportCsv('./donnees/data/synop.201301.csv.gz'),
                     Jointure(reg, ['ID', 'numer_sta']),
                     SelectionVariables(
                         ['ID', 'Nom', 'Region', 't', 'rafper']),
                     Plot('t', 'rafper'),
                     Boxplot(['t', 'rafper']),
                     ExportCsv('mainjulie')])
tab1 = tab1_csv.appliquer()