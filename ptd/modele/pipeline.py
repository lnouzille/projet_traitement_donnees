from ptd.importation.importcsv import ImportCsv
from ptd.importation.importjson import ImportJson
from ptd.transformation.agregationspatiale import AgregationSpatiale
from ptd.transformation.centrage import Centrage
from ptd.transformation.fenetrage import Fenetrage
from ptd.transformation.jointure import Jointure
from ptd.transformation.moyenneglissante import MoyenneGlissante
from ptd.transformation.normalisation import Normalisation
from ptd.transformation.selectionvariables import SelectionVariables
from ptd.exportation.exportcsv import ExportCsv
from ptd.graphique.plot import Plot
from ptd.graphique.boxplot import Boxplot


class Pipeline:
    """ Classe permettant d'executer un pipeline 

    Un pipeline est une séquence d'étapes à réaliser successivement et automatiquement par le programme

    Attributes
    ----------
    - etapes : list 
        Liste des étapes, et donc des classes et de leurs attributs à appeler pour qu'elles soient traitées 
        automatiquement par le programme

    Parameters
    ----------
    - etapes : list 
        Liste des étapes, et donc des classes et de leurs attributs à appeler pour qu'elles soient traitées 
        automatiquement par le programme
    """

    def __init__(self, etapes):
        """ Constructeur

        Parameters
        ----------
        etapes : list 
            Liste des étapes, et donc des classes et de leurs attributs à appeler pour qu'elles soient traitées 
            automatiquement par le programme
        """
        self.__etapes = etapes

    def appliquer(self):
        """ Permet de réaliser successivement les opérations du pipeline 

        Returns 
        -------
        table : Table 
            Table finale dans laquelle toutes les transformations ont été réalisées
        """
        # On s'assure qu'une table a été importée
        if 'Import' not in str(self.__etapes[0]):
            raise Exception(
                'Vous n\'avez renseigné aucune table à importer pour qu\'elle puisse être traitée')
        table = self.__etapes[0].appliquer()
        del self.__etapes[0]
        # On s'assure qu'une seule table a été importée par pipeline
        if len(self.__etapes) and ('Import' in str(self.__etapes[0])):
            raise Exception(
                'Une seul jeu de données ne peut être traité pour un pipeline')
        # On réalise chaque des opétations du pipeline successivement
        for etape in self.__etapes:
            # La méthode appliquer() de ExportCsv ne renvoie pas de table
            if 'ExportCsv' not in str(etape):
                table = etape.appliquer(table)
            else:
                etape.appliquer(table)
        return table
