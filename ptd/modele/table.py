class Table:
    """ Création d'une table 

    Attributes
    ---------- 
    - header : list 
        Nom des colonnes d'une table 
    - body : list 
        Ensemble des données d'une table 

    Parameters
    ---------- 
    - header : list 
        Nom des colonnes d'une table 
    - body : list 
        Ensemble des données d'une table 

    Examples
    --------
    >>> a = Table(['Col1', 'Col2', 'Col3'], [['1', '1', '1'],['2', '2', '2']])
    >>> print(a)
    Col1 Col2 Col3
    ['1', '1', '1']
    ['2', '2', '2']
    <BLANKLINE>
    """

    def __init__(self, header, body):
        """ Constructeur 

        Parameters
        ---------- 
        header : list 
            Nom des colonnes d'une table 
        body : list 
            Ensemble des données d'une table 
        """
        self.__header = header
        self.__body = body

    def header(self):
        """ Renvoie l'en-tête d'une table 

        Returns
        -------
        header : list 
            Nom des colonnes d'une table 

        Examples
        --------
        >>> a = Table(['Col1', 'Col2', 'Col3'], [['1', '1', '1'],['2', '2', '2']])
        >>> a.header()
        ['Col1', 'Col2', 'Col3']
        """
        return self.__header

    def body(self):
        """ Renvoie le corps d'une table 

        Returns
        -------
        body : list 
            Corps d'une table sans son en-tête (c'est-à-dire le nom des colonnes)

        Examples
        --------
        >>> a = Table(['Col1', 'Col2', 'Col3'], [['1', '1', '1'],['2', '2', '2']])
        >>> a.body()
        [['1', '1', '1'], ['2', '2', '2']]
        """
        return self.__body

    def __str__(self):
        """ Chaîne décrivant la table 

        Returns
        -------
        str
            Description de la table
        """
        chaine = " ".join(self.__header) + "\n"
        for i in range(len(self.__body)):
            chaine += "".join(str(self.__body[i])) + "\n"
        return chaine


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
