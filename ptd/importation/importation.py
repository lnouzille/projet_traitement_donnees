from abc import ABC, abstractmethod


class Importation(ABC):
    """ Classe abstraite permettant d'importer un fichier de type .csv, .csv.gz ou json.gz """

    @abstractmethod
    def appliquer(self):
        """ Permet d'importer un fichier de type .csv, .csv.gz ou json.gz """
        pass
