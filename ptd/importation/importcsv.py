from ptd.importation.importation import Importation
from ptd.modele.table import Table
import gzip
import csv


class ImportCsv(Importation):
    """ Classe permettant d'importer un fichier qui est au format csv. et .csv.gz

    Attributes
    ----------
    - chemin : str
        Chemin du fichier à télécharger

    Parameters
    ----------
    - chemin : str
        Chemin du fichier à télécharger

    Examples
    --------
    >>> test = ImportCsv("P:\projet_traitement_donnees\synop.201301.csv.gz")
    >>> print(type(test.appliquer()))
    <class 'table.Table'>
    """

    def __init__(self, chemin):
        """ Constructeur

        Parameters
        ----------
        chemin : str
            chemin du fichier à télécharger
        """
        self.__chemin = chemin

    def appliquer(self):
        """ Importation du fichier .csv ou .csv.gz

        Returns
        -------
        Table
            Jeu de données importé 
        """
        folder = ''
        filename = self.__chemin
        data = []
        if self.__chemin[-2:] == "gz":  # Si le fichier est compressé en .gz
            with gzip.open(folder + filename, mode='rt') as gzfile:
                synopreader = csv.reader(gzfile, delimiter=';')
                for row in synopreader:
                    data.append(row)
        else:  # Si le fichier est en csv sans compression
            with open(folder + filename, mode='rt') as csvfile:
                synopreader = csv.reader(csvfile, delimiter=';')
                for row in synopreader:
                    data.append(row)
        return Table(data[0], data[1:])


if __name__ == '__main__':
    test = ImportCsv("./donnees/data/synop.201301.csv.gz")
    print(test.appliquer())
