from ptd.importation.importation import Importation
from ptd.modele.table import Table
import gzip
import json


class ImportJson(Importation):
    """ Classe permettant d'importer un fichier qui est au format .json.gz

    Attributes
    ----------
    - chemin : str
        Chemin du fichier à télécharger

    Parameters
    ----------
    - chemin : str
        Chemin du fichier à télécharger
    """

    def __init__(self, chemin):
        """ Constructeur

        Parameters
        ----------
        chemin : str
            Chemin du fichier à télécharger
        """
        self.__chemin = chemin

    def appliquer(self):
        """ Importation du fichier .json

        Returns
        -------
        Table
            Jeu de données importé 
        """
        folder = ""
        filename = self.__chemin
        with gzip.open(folder + filename, mode='rt', encoding="utf8") as gzfile:
            data = json.load(gzfile)
        # On cherche la ligne du jeu de données qui va avoir le plus de données pour les variables décrites
        maximum = 0
        header = []
        for i in range(len(data)):
            dictionnaire = data[i]['fields']
            if maximum < len(dictionnaire.keys()):
                maximum = len(dictionnaire.keys())
                header = list(dictionnaire.keys())
        # On construit notre jeu de données en reprenant chacune des lignes
        # et en remplaçant par une valeur manquante 'mq' les données non présentes pour une variable
        body = []
        for i in range(len(data)):
            row = []
            dictionnaire = data[i]['fields']
            for j in header:
                if j in list(dictionnaire.keys()):
                    row.append(dictionnaire[j])
                else:
                    row.append('mq')
            body.append(row)
        return Table(header, body)


if __name__ == '__main__':
    test = ImportJson('./donnees/data/2013-01.json.gz')
    a = test.appliquer()
    print(a)
    print(a.header())
