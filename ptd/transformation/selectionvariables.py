from ptd.transformation.transformation import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy


class SelectionVariables(Transformation):
    """ Sélectionne certaines variables d'un jeu de données 

    Autrement dit, on sélectionne des colonnes dans un jeu de données 

    Attributes
    ----------             
    - variables : list 
        Liste des variables du tableau de données à sélectionner

    Parameters
    ----------             
    - variables : list 
        Liste des variables du tableau de données à sélectionner

    Examples
    --------
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', 17, 11, 'mq'],['12/05/2022', 'Deux-Sèvres', 'mq', 100, 400],['12/05/2022', 'Viennes', 10, 56, 200]])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 17, 11, 'mq']
    ['12/05/2022', 'Deux-Sèvres', 'mq', 100, 400]
    ['12/05/2022', 'Viennes', 10, 56, 200]
    <BLANKLINE>
    >>> t = SelectionVariables(['Departement', 'Consommation gaz'])
    >>> print(t.appliquer(a))
    Departement Consommation gaz
    ['Charente-Maritime', 'mq']
    ['Deux-Sèvres', 400]
    ['Viennes', 200]
    <BLANKLINE>
    """

    def __init__(self, variables):
        """ Constructeur 

        Parameters
        ----------             
        variables : list 
            Liste des variables du tableau de données à sélectionner
        """
        self.__variables = variables

    def appliquer(self, data):
        """ Fonction pour retourner que certaines variables sélectionnées.

        Parameters
        ----------
        data : Table
            Tableau de données sous forme de listes de listes sur lequel on effectue la sélection des colonnes.

        Returns
        -------
        Table
            Table avec uniquement les colonnes sélectionnées 
        """
        colonnes_selection = []
        # On extrait les colonnes sélectionnées
        for var in self.__variables:
            if var not in data.header():  # Si l'une des variables renseignée n'est pas dans l'en-tête de la table
                raise Exception(
                    'La selection de variables n\'a pas pu aboutir puisque l\'une des variables n\'est pas présente dans le jeu de données.')
            colonnes_selection.append(OutilsStats(
                data.header()).selectionCol(data.body(), var))
        # On les ajoute au body
        body = [[] for i in range(len(colonnes_selection[0]))]
        for col in range(len(colonnes_selection)):
            for row in range(len(colonnes_selection[col])):
                body[row].append(colonnes_selection[col][row])
        return Table(self.__variables, body)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
