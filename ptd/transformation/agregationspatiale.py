from ptd.transformation.transformation import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy


class AgregationSpatiale(Transformation):
    """ Application d'une agrégation spatiale des données 

    L'agrégation spatiale consiste ici à passer d'une granularité régionale à nationale 
    en réalisant deux types d'agrégation possibles : la somme ou la moyenne nationale 

    Attributes
    ----------
    - type_agregation : str, default 'somme'
        Type d'agrégation (somme ou moyenne)

    Parameters
    ----------
    - type_agregation : str, default 'somme'
        Type d'agrégation (somme ou moyenne)

    Examples
    --------
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', '100'],['12/05/2022', 'Deux-Sèvres', 'mq', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', '17', '11', '100']
    ['12/05/2022', 'Deux-Sèvres', 'mq', '100', '400']
    ['12/05/2022', 'Viennes', '10', '56', '200']
    <BLANKLINE>
    >>> t = AgregationSpatiale('somme')
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'National', 27.0, 167.0, 700.0]
    <BLANKLINE>
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', '100'],['14/05/2022', 'Deux-Sèvres', 'mq', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', '17', '11', '100']
    ['14/05/2022', 'Deux-Sèvres', 'mq', '100', '400']
    ['12/05/2022', 'Viennes', '10', '56', '200']
    <BLANKLINE>
    >>> t = AgregationSpatiale('moyenne')
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['indéfinie', 'National', 13.5, 55.667, 233.333]
    <BLANKLINE>
    """

    def __init__(self, type_agregation='somme'):
        """ Constructeur 

        Parameters
        ----------
        type_agregation : str, default 'somme'
            Type d'agrégation (somme ou moyenne)
        """
        self.__type = type_agregation

    def appliquer(self, data):
        """ Agrège les données d'un jeu de données avec le type d'agrégation renseigné

        Parameters
        ----------
        data : Table 
            Jeu de données sur lequel effectuer l'agrégation des données 

        Returns
        -------
        Table 
            Jeu de données sur lequel l'agrégation des données a été effectuée
        """
        body = []
        for i in range(len(data.header())):
            # On récupère à chaque boucle la colonne de la table sur laquelle on va devoir réaliser l'agrégation
            col = OutilsStats(data.header()).selectionCol(
                data.body(), data.header()[i])
            col_float = OutilsStats(col)
            col_float.convertirFloat()
            col_float = col_float.data()
            # On traite les différents cas possible en fonction du type et du sens des variables
            if data.header()[i].lower() in ('departement', 'region', 'commune'):
                body.append('National')
            elif 'date' in data.header()[i].lower():
                # Toutes les données ont la même date
                if col_float.count(col_float[0]) == len(col_float):
                    body.append(col_float[0])
                else:
                    body.append('indéfinie')
            elif data.header()[i].lower() == 'id':
                body.append('id')
            else:
                if self.__type.lower() == 'somme':
                    body.append(OutilsStats(col_float).somme())
                elif self.__type.lower() == 'moyenne':
                    body.append(round(OutilsStats(col_float).moyenne(), 3))
                else:
                    raise Exception(
                        'Le type d\'agrégation entré ne peut pas être traité.')
        return Table(data.header(), [body])


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
