from ptd.transformation.transformation import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy


class MoyenneGlissante(Transformation):
    """ Classe de type de moyenne statistique utilisée pour analyser des séries ordonnées de données

    Attributes
    ---------- 
    - variables : list
        Liste des variables où on applique la moyenne glissante
    - taillefenetre : int, default 3
        Nombre indiquant sur combien de données la moyenne glissante doit être effectuée

    Parameters
    ---------- 
    - variables : list
        Liste des variables où on applique la moyenne glissante
    - taillefenetre : int, default 3
        Nombre indiquant sur combien de données la moyenne glissante doit être effectuée

    Examples 
    ----------
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', 17, 11, 100],['12/05/2022', 'Deux-Sèvres', 14, 100, 400],['12/05/2022', 'Viennes', 10, 56, 200]])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 17, 11, 100]
    ['12/05/2022', 'Deux-Sèvres', 14, 100, 400]
    ['12/05/2022', 'Viennes', 10, 56, 200]
    <BLANKLINE>
    >>> t = MoyenneGlissante(['Temperature','Consommation gaz'])
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 15.5, 11, 250.0]
    ['12/05/2022', 'Deux-Sèvres', 13.667, 100, 233.333]
    ['12/05/2022', 'Viennes', 12.0, 56, 300.0]
    <BLANKLINE>
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', 'mq', 11, 100],['12/05/2022', 'Deux-Sèvres', 14, 100, 400],['12/05/2022', 'Viennes', 10, 56, 200]])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 'mq', 11, 100]
    ['12/05/2022', 'Deux-Sèvres', 14, 100, 400]
    ['12/05/2022', 'Viennes', 10, 56, 200]
    <BLANKLINE>
    >>> t = MoyenneGlissante(['Temperature','Consommation gaz'])
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 14.0, 11, 250.0]
    ['12/05/2022', 'Deux-Sèvres', 12.0, 100, 233.333]
    ['12/05/2022', 'Viennes', 12.0, 56, 300.0]
    <BLANKLINE>
    """

    def __init__(self, variables, taillefenetre=3):
        """ Constructeur 

        Parameters
        ---------- 
        variables : list
            Liste des variables où on applique la moyenne glissante
        taillefenetre : int, default 3
            Nombre indiquant sur combien de données la moyenne glissante doit être effectuée
        """
        self.__variables = variables
        self.__taillefenetre = taillefenetre

    def extraire(self, col, indice=0):
        """ Permet d'extraire les données en fonction de la taille de la fenetre

        Parameters
        ----------
        col : list
            La colonne sélectionnée
        indice : int, default 0 
            Indice de la valeur à partir de laquelle l'extraction doit être réalisée 

        Returns
        -------
        extrait : list
            La liste extraite
        """
        p = int((self.__taillefenetre - 1)//2)
        indice_debut = indice - p
        indice_fin = indice + p
        if indice_debut < 0:
            indice_debut = 0
            extrait = col[indice_debut:indice_fin+1]
        if indice_fin > len(col):
            extrait = col[indice_debut:]
        else:
            extrait = col[indice_debut:indice_fin+1]
        return extrait

    def moy_glissante(self, col):
        """ Permet de calculer la moyenne glissante sur une colonne

        Parameters
        ----------
        col : list
            La colonne sélectionnée

        Returns
        -------
        list
            La nouvelle liste avec les valeurs qui ont été transformées par leur moyenne glissante 
        """
        liste = []
        for i in range(len(col)):
            extraction = self.extraire(col, i)
            moy = round(OutilsStats(extraction).moyenne(), 3)
            liste.append(moy)
        return liste

    def appliquer(self, data):
        """ Effectue la moyenne glissante sur les données d'un jeu de données pour les variables renseignées 

        Parameters
        ----------
        data : Table 
            Jeu de données sur lequel effectuer la moyenne glissante de certaines variables 

        Returns 
        ----------
        Table
            Jeu de données où la moyenne glissante a été effectuée      
        """
        body = copy(data.body())
        for var in self.__variables:
            if var not in data.header():  # Si l'une des variables renseignée n'est pas dans l'en-tête de la table
                raise Exception(
                    'La moyenne glissante n\'a pas pu aboutir puisque l\'une des variables n\'est pas présente dans le jeu de données.')
            col = OutilsStats(data.header()).selectionCol(data.body(), var)
            # On effectue la moyenne glissante de la colonne
            moyenneglissante = self.moy_glissante(col)
            num = OutilsStats(data.header()).numeroCol(var)
            for i in range(len(col)):
                body[i][num] = moyenneglissante[i]
        return Table(data.header(), body)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
