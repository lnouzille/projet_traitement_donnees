from ptd.transformation.transformation import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy


class Centrage(Transformation):
    """ Application de la transformation du centrage sur un jeu de données

    Classe permettant de centrer les données (retirer la moyenne) pour une liste de variables données 

    Attributes
    ---------- 
    - variables : list
        Liste des variables à centrer 

    Parameters
    ---------- 
    - variables : list
        Liste des variables à centrer 

    Examples
    --------
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', '100'],['12/05/2022', 'Deux-Sèvres', '14', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', '17', '11', '100']
    ['12/05/2022', 'Deux-Sèvres', '14', '100', '400']
    ['12/05/2022', 'Viennes', '10', '56', '200']
    <BLANKLINE>
    >>> t = Centrage(['Temperature', 'Consommation gaz'])
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 3.333, '11', -133.333]
    ['12/05/2022', 'Deux-Sèvres', 0.333, '100', 166.667]
    ['12/05/2022', 'Viennes', -3.667, '56', -33.333]
    <BLANKLINE>
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', '100'],['12/05/2022', 'Deux-Sèvres', 'mq', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', '17', '11', '100']
    ['12/05/2022', 'Deux-Sèvres', 'mq', '100', '400']
    ['12/05/2022', 'Viennes', '10', '56', '200']
    <BLANKLINE>
    >>> t = Centrage(['Temperature', 'Consommation gaz'])
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 3.5, '11', -133.333]
    ['12/05/2022', 'Deux-Sèvres', 'mq', '100', 166.667]
    ['12/05/2022', 'Viennes', -3.5, '56', -33.333]
    <BLANKLINE>
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', '100'],['12/05/2022', 'Deux-Sèvres', ' ', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', '17', '11', '100']
    ['12/05/2022', 'Deux-Sèvres', ' ', '100', '400']
    ['12/05/2022', 'Viennes', '10', '56', '200']
    <BLANKLINE>
    >>> t = Centrage(['Temperature', 'Consommation gaz'])
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 3.5, '11', -133.333]
    ['12/05/2022', 'Deux-Sèvres', ' ', '100', 166.667]
    ['12/05/2022', 'Viennes', -3.5, '56', -33.333]
    <BLANKLINE>
    """

    def __init__(self, variables):
        """ Constructeur 

        Parameters
        ---------- 
        variables : list
            Liste des variables à centrer 
        """
        self.__variables = variables

    def variables(self):
        """ Permet de recupérer la valeur de l'attribut privé contenant la liste des variables à centrer 

        Returns
        -------
        variables : list
            Liste des variables à centrer 

        Examples
        --------
        >>> v = Centrage([1, 2, 3])
        >>> v.variables()
        [1, 2, 3]
        """
        return self.__variables

    def appliquer(self, data):
        """ Centre les données d'un jeu de données pour les variables renseignées 

        Parameters
        ----------
        data : Table 
            Jeu de données sur lequel effectuer le centrage de certaines variables 

        Returns
        -------
        Table 
            Jeu de données sur lequel le centrage des données a été effectué 
        """
        body = copy(data.body())
        for var in self.__variables:
            if var not in data.header():  # Si l'une des variables renseignée n'est pas dans l'en-tête de la table
                raise Exception(
                    'Le centrage n\'a pas pu aboutir puisque l\'une des variables n\'est pas présente dans le jeu de données.')
            num = OutilsStats(data.header()).numeroCol(var)
            col = OutilsStats(data.header()).selectionCol(data.body(), var)
            longueur = len(col)
            # Calcul de la moyenne de la colonne
            moy = OutilsStats(col).moyenne()
            for row in range(longueur):
                # Centre une valeur que si celle-ci n'est pas une donnée manquante
                if body[row][num] not in ('mq', ' '):
                    body[row][num] = round(float(body[row][num]) - moy, 3)
        return Table(data.header(), body)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
