from ptd.transformation.transformation import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy
from datetime import datetime


class Fenetrage(Transformation):
    """ Sélectionne des données d'un jeu de données sur une période temporelle

    Attributes
    ---------- 
    - date_debut : str
        Date de début de la période temporelle 
    - date_fin : str 
        Date de fin de la période temporelle 

    Parameters
    ---------- 
    - date_debut : str
        Date de début de la période temporelle
    - date_fin : str 
        Date de fin de la période temporelle  

    Note importante
    ---------------
    Pour sélectionner qu'une journée, il faut préciser dans la date de début, la date souhaitée, 
    et en date de fin, la date du lendemain pour que toutes les heures de la journée soient prises en considération. 
    Autrement, veuillez préciser les heures précises de la journée.  

    Examples
    --------
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['2022-11-04', 'Charente-Maritime', 17, 11, 100],['2021-01-17', 'Deux-Sèvres', 14, 100, 400],['2022-12-25', 'Viennes', 10, 56, 200]])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['2022-11-04', 'Charente-Maritime', 17, 11, 100]
    ['2021-01-17', 'Deux-Sèvres', 14, 100, 400]
    ['2022-12-25', 'Viennes', 10, 56, 200]
    <BLANKLINE>
    >>> t = Fenetrage('2022-03-01', '2022-12-31')
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['2022-11-04', 'Charente-Maritime', 17, 11, 100]
    ['2022-12-25', 'Viennes', 10, 56, 200]
    <BLANKLINE>
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['2011-11-04 00:00:00', 'Charente-Maritime', 17, 11, 100],['2011-11-04 18:00:00', 'Deux-Sèvres', 14, 100, 400],['2011-11-05 01:00:00', 'Viennes', 10, 56, 200]])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['2011-11-04 00:00:00', 'Charente-Maritime', 17, 11, 100]
    ['2011-11-04 18:00:00', 'Deux-Sèvres', 14, 100, 400]
    ['2011-11-05 01:00:00', 'Viennes', 10, 56, 200]
    <BLANKLINE>
    >>> t = Fenetrage('2011-11-04', '2011-11-05')
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['2011-11-04 00:00:00', 'Charente-Maritime', 17, 11, 100]
    ['2011-11-04 18:00:00', 'Deux-Sèvres', 14, 100, 400]
    <BLANKLINE>
    """

    def __init__(self, date_debut, date_fin):
        """ Constructeur 

        Parameters
        ---------- 
        date_debut : str
            Date de début de la période temporelle 
        date_fin : str 
            Date de fin de la période temporelle
        """
        # On convertit directement les dates str au format datetime
        self.__date_debut = self.date_time(date_debut)
        self.__date_fin = self.date_time(date_fin)

    def date_time(self, date):
        """ Mettre une date de chaine de caractères au format date.time 

        Parameters
        ----------
        date : str 
            Date à convertir 

        Returns 
        -------
        Datetime 
            Date convertie au bon format 

        Information sur datetime
        ------------------------
        class datetime.datetime(year, month, day, hour=0, minute=0, second=0, microsecond=0, tzinfo=None, *, fold=0)

        Examples
        --------
        >>> a = Fenetrage('2011-11-04', '2014-12-05')
        >>> print(a.date_time('2013-11-04T18:00:00+01:00'))
        2013-11-04 18:00:00+01:00
        >>> print(a.date_time('2011-11-04'))
        2011-11-04 00:00:00
        >>> print(a.date_time('20111104184500'))
        2011-11-04 18:45:00
        """
        try:
            return datetime.fromisoformat(date)  # Format du json
        except:
            return datetime.strptime(date, "%Y%m%d%H%M%S")  # Format du csv

    def appliquer(self, data):
        """ Sélectionne des données d'un jeu de données sur une période temporelle

        Parameters
        ----------
        data : Table 
            Jeu de données sur lequel effectuer la sélection 

        Returns
        -------
        Table 
            Jeu de données sur lequel les données d'une période temporelle ont été sélectionnées 
        """
        # On vérifie que la date de début de la période est bien inférieure à celle de fin
        if self.__date_debut > self.__date_fin:
            raise Exception(
                'Période temporelle incorrecte')
        # Suppression des majuscules de l'en-tête du jeu de données
        header = copy(data.header())
        for i in range(len(header)):
            header[i] = header[i].lower()
        # On gère le cas où date n'a pas été selectionnée mais date_heure oui et on recupère le numéro de la colonne date
        try:
            numero_col_date = OutilsStats(header).numeroCol('date')
        except:
            numero_col_date = OutilsStats(header).numeroCol('date_heure')
        # On sélectionne que les données comprises entre la période temporelle souhaitée
        body = []
        for row in range(len(data.body())):
            if self.date_time(data.body()[row][numero_col_date]) >= self.__date_debut and self.date_time(data.body()[row][numero_col_date]) <= self.__date_fin:
                body.append(data.body()[row])
        return Table(data.header(), body)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
