from abc import ABC, abstractmethod


class Transformation(ABC):
    """ Effectue la transformation d'une table """

    @abstractmethod
    def appliquer(self, data):
        """ Applique la transformation demandée à l'ensemble de la table """
        pass
