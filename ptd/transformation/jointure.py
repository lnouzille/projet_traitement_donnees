from ptd.transformation.transformation import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy


class Jointure(Transformation):
    """ Jointure de deux tables 

    La jointure de deux tables s'effectue en fonction du nom des colonnes renseignées et du type de jointure 

    Attributes
    ----------
    - table : Table 
        Jeu de données à joindre 
    - colonne : list
        Nom des deux colonnes sur lesquelles effectuer la jointure
    - type_jointure : str, default = 'inner'
        Type de jointure à effectuer 

    Parameters
    ----------
    - table : Table 
        Jeu de données à joindre 
    - colonne : list
        Nom des deux colonnes sur lesquelles effectuer la jointure
    - type_jointure : str, default = 'inner'
        Type de jointure à effectuer 

    Note importante
    ---------------
    Soient a et b deux Tables à joindre et la commande : Jointure(b, 'Departement', 'left').appliquer(a).
    Cette commande va sélectionner toutes les colonnes de b et la joindre avec a ou remplacer les éléments de a par des données 
    manquantes lorsque ceux-ci n'existent pas. 
    A l'inverse, la commande Jointure(b, 'Departement', 'right').appliquer(a) va sélectionner toutes les colonnes de a et
    la joindre avec b ou remplacer les éléments de b par des données manquantes lorsque ceux-ci n'existent pas. 

    Examples
    --------
    >>> print("Inner join")
    Inner join
    >>> a = Table(['Date','Departement','Temperature','Densite'], [['2022-11-04', 'Charente-Maritime', 17, 11],['2021-01-17', 'Deux-Sèvres', 14, 100],['2022-12-25', 'Viennes', 10, 56]])
    >>> print(a)
    Date Departement Temperature Densite
    ['2022-11-04', 'Charente-Maritime', 17, 11]
    ['2021-01-17', 'Deux-Sèvres', 14, 100]
    ['2022-12-25', 'Viennes', 10, 56]
    <BLANKLINE>
    >>> b = Table(['Departement2','Consommation gaz'], [['Deux-Sèvres', 100],['Charente-Maritime', 400],['Viennes', 200]])
    >>> print(b)
    Departement2 Consommation gaz
    ['Deux-Sèvres', 100]
    ['Charente-Maritime', 400]
    ['Viennes', 200]
    <BLANKLINE>
    >>> t = Jointure(b, ['Departement2','Departement'], 'inner')
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Departement2 Consommation gaz
    ['2022-11-04', 'Charente-Maritime', 17, 11, 'Charente-Maritime', 400]
    ['2021-01-17', 'Deux-Sèvres', 14, 100, 'Deux-Sèvres', 100]
    ['2022-12-25', 'Viennes', 10, 56, 'Viennes', 200]
    <BLANKLINE>
    >>> a = Table(['Date','Departement','Temperature','Densite'], [['2022-11-04', 'Charente-Maritime', 17, 11],['2021-01-17', 'Deux-Sèvres', 14, 100],['2022-12-25', 'Viennes', 10, 56]])
    >>> print(a)
    Date Departement Temperature Densite
    ['2022-11-04', 'Charente-Maritime', 17, 11]
    ['2021-01-17', 'Deux-Sèvres', 14, 100]
    ['2022-12-25', 'Viennes', 10, 56]
    <BLANKLINE>
    >>> b = Table(['Departement2','Consommation gaz'], [['Deux-Sèvres', 100],['Charente-Maritime', 400]])
    >>> print(b)
    Departement2 Consommation gaz
    ['Deux-Sèvres', 100]
    ['Charente-Maritime', 400]
    <BLANKLINE>
    >>> t = Jointure(b, ['Departement2','Departement'], 'inner')
    >>> print(t.appliquer(a))
    Departement2 Consommation gaz Date Departement Temperature Densite
    ['Deux-Sèvres', 100, '2021-01-17', 'Deux-Sèvres', 14, 100]
    ['Charente-Maritime', 400, '2022-11-04', 'Charente-Maritime', 17, 11]
    <BLANKLINE>
    >>> print("Left join")
    Left join
    >>> a = Table(['Date','Departement','Temperature','Densite'], [['2022-11-04', 'Charente-Maritime', 17, 11],['2022-12-25', 'Viennes', 10, 56]])
    >>> print(a)
    Date Departement Temperature Densite
    ['2022-11-04', 'Charente-Maritime', 17, 11]
    ['2022-12-25', 'Viennes', 10, 56]
    <BLANKLINE>
    >>> b = Table(['Departement2','Consommation gaz'], [['Deux-Sèvres', 100],['Charente-Maritime', 400],['Viennes', 200]])
    >>> print(b)
    Departement2 Consommation gaz
    ['Deux-Sèvres', 100]
    ['Charente-Maritime', 400]
    ['Viennes', 200]
    <BLANKLINE>
    >>> t = Jointure(b, ['Departement2','Departement'], 'left')
    >>> print(t.appliquer(a))
    Departement2 Consommation gaz Date Departement Temperature Densite
    ['Deux-Sèvres', 100, 'mq', 'mq', 'mq', 'mq']
    ['Charente-Maritime', 400, '2022-11-04', 'Charente-Maritime', 17, 11]
    ['Viennes', 200, '2022-12-25', 'Viennes', 10, 56]
    <BLANKLINE>
    >>> print("Right join")
    Right join
    >>> a = Table(['Date','Departement','Temperature','Densite'], [['2022-11-04', 'Charente-Maritime', 17, 11],['2021-01-17', 'Deux-Sèvres', 14, 100],['2022-12-25', 'Viennes', 10, 56]])
    >>> print(a)
    Date Departement Temperature Densite
    ['2022-11-04', 'Charente-Maritime', 17, 11]
    ['2021-01-17', 'Deux-Sèvres', 14, 100]
    ['2022-12-25', 'Viennes', 10, 56]
    <BLANKLINE>
    >>> b = Table(['Departement2','Consommation gaz'], [['Charente-Maritime', 400],['Viennes', 200]])
    >>> print(b)
    Departement2 Consommation gaz
    ['Charente-Maritime', 400]
    ['Viennes', 200]
    <BLANKLINE>
    >>> t = Jointure(b, ['Departement2','Departement'], 'right')
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Departement2 Consommation gaz
    ['2022-11-04', 'Charente-Maritime', 17, 11, 'Charente-Maritime', 400]
    ['2021-01-17', 'Deux-Sèvres', 14, 100, 'mq', 'mq']
    ['2022-12-25', 'Viennes', 10, 56, 'Viennes', 200]
    <BLANKLINE>
    """

    def __init__(self, table, colonne, type_jointure='inner'):
        """ Contructeur

        Parameters
        ----------
        table : Table 
            Jeu de données à joindre 
        colonne : list
            Nom des deux colonnes sur lesquelles effectuer la jointure
        type_jointure : str, default = 'inner'
            Type de jointure à effectuer
        """
        self.__table = table
        if len(colonne) != 2:
            raise Exception(
                'La variable colonne doit être une liste des noms des deux colonnes des deux tables sur lesquelles faire la jointure.')
        self.__colonne = colonne
        self.__type = type_jointure

    def appliquer(self, data):
        """ Effectue la jointure de deux jeux de données

        La jointure de deux jeux de données s'effectue en fonction du nom de la colonne renseignée et du type de jointure 

        Parameters
        ----------
        data : Table 
            Jeu de données sur lequel effectuer la jointure 

        Returns
        -------
        Table 
            Jeu de données sur lequel deux tables ont été jointes 
        """
        # Choix de départ de l'ordre des tables en fontion du type de jointure
        if self.__type == "inner":
            if len(data.body()) > len(self.__table.body()):
                tab_left, tab_right = self.__table, data
                c1, c2 = 0, 1
            else:
                tab_left, tab_right = data, self.__table
                c1, c2 = 1, 0
        elif self.__type == "left":
            tab_left, tab_right = self.__table, data
            c1, c2 = 0, 1
        elif self.__type == "right":
            tab_left, tab_right = data, self.__table
            c1, c2 = 1, 0
        else:
            raise(Exception("Le type de jointure renseigné est incorrect."))
        # On vérifie que les noms de colonnes sont corrects
        if (self.__colonne[c1] not in tab_left.header()) or (self.__colonne[c2] not in tab_right.header()):
            raise Exception('Les noms de colonnes entrés sont incorrects.')
        # On recupère les colonnes de jointure des deux tables
        col_tab_left = OutilsStats(tab_left.header()).selectionCol(
            tab_left.body(), self.__colonne[c1])
        col_tab_right = OutilsStats(tab_right.header()).selectionCol(
            tab_right.body(), self.__colonne[c2])
        # On construit le nouveau jeu de données
        # Header
        header = copy(tab_left.header())
        for i in range(len(tab_right.header())):
            element = tab_right.header()[i]
            header.append(element)
        # Body
        body = []
        for row in range(len(tab_left.body())):
            ligne = copy(tab_left.body()[row])
            # On gère le cas où les deux colonnes de jointure sont ordonnées différement
            num_ligne = OutilsStats(col_tab_right).numeroCol(col_tab_left[row])
            # Cas de la jointure à gauche et à droite :
            # On doit remplacer les valeurs par des données manquantes lorsqu'elles n'existent pas
            if num_ligne is None and self.__type != 'inner':
                valeurs = ['mq']*len(tab_right.header())
            elif num_ligne is not None:
                valeurs = copy(tab_right.body()[num_ligne])
            if len(valeurs):
                for valeur in valeurs:
                    ligne.append(valeur)
                body.append(ligne)
        return Table(header, body)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
