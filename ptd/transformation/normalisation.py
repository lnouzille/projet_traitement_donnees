from ptd.transformation.transformation import Transformation
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy
from ptd.transformation.centrage import Centrage


class Normalisation(Centrage):
    """ Application de la transformation de la normalisation sur un jeu de données

    Classe permettant de normaliser les données (retirer la moyenne et diviser par l'écart-type) 
    pour une liste de variables données 

    Attributes
    ---------- 
    - variables : list
        Liste des variables à normaliser 

    Parameters
    ---------- 
    - variables : list
        Liste des variables à normaliser

    Examples
    --------
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', '100'],['12/05/2022', 'Deux-Sèvres', '14', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', '17', '11', '100']
    ['12/05/2022', 'Deux-Sèvres', '14', '100', '400']
    ['12/05/2022', 'Viennes', '10', '56', '200']
    <BLANKLINE>
    >>> t = Normalisation(['Temperature','Consommation gaz'])
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 1.162, '11', -1.069]
    ['12/05/2022', 'Deux-Sèvres', 0.116, '100', 1.336]
    ['12/05/2022', 'Viennes', -1.279, '56', -0.267]
    <BLANKLINE>
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', 'mq'],['12/05/2022', 'Deux-Sèvres', 'mq', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', '17', '11', 'mq']
    ['12/05/2022', 'Deux-Sèvres', 'mq', '100', '400']
    ['12/05/2022', 'Viennes', '10', '56', '200']
    <BLANKLINE>
    >>> t = Normalisation(['Temperature','Consommation gaz'])
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 1.0, '11', 'mq']
    ['12/05/2022', 'Deux-Sèvres', 'mq', '100', 1.0]
    ['12/05/2022', 'Viennes', -1.0, '56', -1.0]
    <BLANKLINE>
    >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', ' '],['12/05/2022', 'Deux-Sèvres', ' ', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
    >>> print(a)
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', '17', '11', ' ']
    ['12/05/2022', 'Deux-Sèvres', ' ', '100', '400']
    ['12/05/2022', 'Viennes', '10', '56', '200']
    <BLANKLINE>
    >>> t = Normalisation(['Temperature','Consommation gaz'])
    >>> print(t.appliquer(a))
    Date Departement Temperature Densite Consommation gaz
    ['12/05/2022', 'Charente-Maritime', 1.0, '11', ' ']
    ['12/05/2022', 'Deux-Sèvres', ' ', '100', 1.0]
    ['12/05/2022', 'Viennes', -1.0, '56', -1.0]
    <BLANKLINE>
    """

    def __init__(self, variables):
        """ Constructeur 

        Parameters
        ---------- 
        variables : list
            Liste des variables à normaliser
        """
        super().__init__(variables)

    def centrer(self, data):
        """ Permet de centrer le jeu de données 

        Parameters 
        ----------
        data : Table
            Jeu de données sur lequel appliquer la transformation de centrage pour les variables concernées

        Returns
        -------
        Table 
            Jeu de données sur lequel le centrage des données pour les variables concernées a été effectué 

        Examples
        --------
        >>> a = Table(['Date','Departement','Temperature','Densite','Consommation gaz'], [['12/05/2022', 'Charente-Maritime', '17', '11', '100'],['12/05/2022', 'Deux-Sèvres', '14', '100', '400'],['12/05/2022', 'Viennes', '10', '56', '200']])
        >>> print(a)
        Date Departement Temperature Densite Consommation gaz
        ['12/05/2022', 'Charente-Maritime', '17', '11', '100']
        ['12/05/2022', 'Deux-Sèvres', '14', '100', '400']
        ['12/05/2022', 'Viennes', '10', '56', '200']
        <BLANKLINE>
        >>> t = Normalisation(['Temperature','Consommation gaz'])
        >>> print(t.centrer(a))
        Date Departement Temperature Densite Consommation gaz
        ['12/05/2022', 'Charente-Maritime', 3.333, '11', -133.333]
        ['12/05/2022', 'Deux-Sèvres', 0.333, '100', 166.667]
        ['12/05/2022', 'Viennes', -3.667, '56', -33.333]
        <BLANKLINE>
        """
        return super().appliquer(data)

    def appliquer(self, data):
        """ Normalise les données d'un jeu de données pour les variables renseignées 

        Parameters 
        ----------
        data : Table
            Jeu de données sur lequel appliquer la transformation de normalisation pour les variables concernées

        Returns
        -------
        Table 
            Jeu de données sur lequel la normalisation des données pour les variables concernées a été effectuée
        """
        body = self.centrer(data).body()
        for var in super().variables():
            if var not in data.header():  # Si l'une des variables renseignée n'est pas dans l'en-tête de la table
                raise Exception(
                    'La normalisation n\'a pas pu aboutir puisque l\'une des variables n\'est pas présente dans le jeu de données.')
            num = OutilsStats(data.header()).numeroCol(var)
            col = OutilsStats(data.header()).selectionCol(data.body(), var)
            longueur = len(col)
            # On calcule l'écart-type de la colonne
            ecarttype = OutilsStats(col).ecarttype()
            for row in range(longueur):
                # Normalise une valeur que si celle-ci n'est pas une donnée manquante
                if body[row][num] not in ('mq', ' '):
                    body[row][num] = round(float(body[row][num])/ecarttype, 3)
        return Table(data.header(), body)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
