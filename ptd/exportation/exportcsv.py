from ptd.exportation.exportation import Exportation
from ptd.modele.table import Table
from ptd.utilitaire.outilsstats import OutilsStats
from copy import copy
import csv


class ExportCsv(Exportation):
    """ Exporter un jeu de données dans un fichier .csv

    Attributes
    ---------- 
    - name : str, default 'fichier'
        Nom du fichier 

    Parameters
    ----------
    - name : str, default 'fichier'
        Nom du fichier 

    Note importante
    ---------------
    Si le nom du fichier renseigné est le même que celui d'un fichier existant, 
    ce dernier sera écrasé par le nouveau fichier exporté 
    """

    def __init__(self, name='fichier'):
        """ Constructeur 

        Parameters
        ----------
        name : str, default 'fichier'
            Nom du fichier 
        """
        self.__name = name

    def convertir_chaine(self, liste):
        """ Convertir une liste en chaine de caractères avec un séparateur point-virgule 

        Convertir une liste en une chaine de caractères à écrire dans un fichier .csv en prenant 
        en compte le séparateur de chaque colonne 

        Parameters
        ----------
        liste : list 
            Liste à convertir en chaine de caractères

        Returns 
        -------
        str 
            Chaine de caractères contenant les éléments d'une liste séparés par un point-virgule 

        Examples
        --------
        >>> t = ExportCsv('fichier')
        >>> print(t.convertir_chaine(["Departement", '1', 2]))
        Departement;1;2
        <BLANKLINE>
        >>> print(t.convertir_chaine(["Departement", 1, 'mq']))
        Departement;1;mq
        <BLANKLINE>
        """
        return ";".join(str(element) for element in liste) + "\n"

    def appliquer(self, data):
        """ Ecrire un fichier .csv avec les informations contenues dans une Table 

        Parameters
        ----------
        data : Table 
            Jeu de données à écrire dans un fichier .csv
        """
        fichier = open("./donnees/sortie/"+self.__name +
                       ".csv", 'w', encoding='utf8')
        head = self.convertir_chaine(data.header())
        fichier.write(head)
        for i in range(len(data.body())):
            ligne = self.convertir_chaine(data.body()[i])
            fichier.write(ligne)
        fichier.close()


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
