from abc import ABC, abstractmethod


class Exportation(ABC):
    """ Classe abstraite permettant d'exporter un jeu de données """

    @abstractmethod
    def appliquer(self, data):
        """ Applique l'export d'un jeu de données """
        pass
