from ptd.importation.importcsv import ImportCsv
from ptd.importation.importjson import ImportJson
from ptd.transformation.agregationspatiale import AgregationSpatiale
from ptd.transformation.centrage import Centrage
from ptd.transformation.fenetrage import Fenetrage
from ptd.transformation.jointure import Jointure
from ptd.transformation.moyenneglissante import MoyenneGlissante
from ptd.transformation.normalisation import Normalisation
from ptd.transformation.selectionvariables import SelectionVariables
from ptd.exportation.exportcsv import ExportCsv
from ptd.graphique.plot import Plot
from ptd.graphique.boxplot import Boxplot
from ptd.modele.pipeline import Pipeline
from ptd.modele.table import Table


# Test 1 : Relation entre température et consommation électrique
reg = Pipeline([ImportCsv('./donnees/data/postesSynopAvecRegions.csv')])
reg = reg.appliquer()
tab1_csv = Pipeline([ImportCsv('./donnees/data/synop.201301.csv.gz'),
                     Jointure(reg, ['ID', 'numer_sta']),
                     SelectionVariables(['ID', 'Nom', 'Region', 't'])])
tab1 = tab1_csv.appliquer()
tab2_json = Pipeline([ImportJson('./donnees/data/2013-01.json.gz'),
                      SelectionVariables(
                          ['region', 'consommation_brute_electricite_rte']),
                      Jointure(tab1, ['Region', 'region']),
                      Plot('t', 'consommation_brute_electricite_rte'),
                      ExportCsv('test1')])
tab2 = tab2_json.appliquer()
# print(tab2)

# Test 2 : Relation entre vent et temperature
reg = Pipeline([ImportCsv('./donnees/data/postesSynopAvecRegions.csv')])
reg = reg.appliquer()
tab1_csv = Pipeline([ImportCsv('./donnees/data/synop.201301.csv.gz'),
                     Jointure(reg, ['ID', 'numer_sta']),
                     SelectionVariables(
                         ['ID', 'Nom', 'Region', 't', 'rafper']),
                     Plot('t', 'rafper'),
                     Boxplot(['t', 'rafper']),
                     ExportCsv('test2')])
tab1 = tab1_csv.appliquer()

# Test 3 : Température moyenne à l'échelle nationale en 2013
reg = Pipeline([ImportCsv('./donnees/data/postesSynopAvecRegions.csv')])
reg = reg.appliquer()
tab1_csv = Pipeline([ImportCsv('./donnees/data/synop.201301.csv.gz'),
                     Jointure(reg, ['ID', 'numer_sta']),
                     SelectionVariables(['ID', 'Nom', 'Region', 't']),
                     AgregationSpatiale('moyenne'),
                     ExportCsv('test3')])
tab1 = tab1_csv.appliquer()
# print(tab1)  # 283,945K = 9,85°C

# Test 4 : Normalisation
tab1_csv = Pipeline([ImportCsv('./donnees/data/synop.201301.csv.gz'),
                     SelectionVariables(['numer_sta', 'date', 'rr24']),
                     Normalisation(['rr24']),
                     ExportCsv('test4')])
tab1 = tab1_csv.appliquer()
# print(tab1)

# Test 5 : Fenêtrage
tab1_csv = Pipeline([ImportCsv('./donnees/data/synop.201301.csv.gz'),
                     SelectionVariables(['numer_sta', 'date', 't']),
                     Fenetrage('2013-01-01', '2013-01-01 23:59:00'),
                     ExportCsv('test5')])
tab1 = tab1_csv.appliquer()
# print(tab1)

# Test 6 : Moyenne glissante
tab2_json = Pipeline([ImportJson('./donnees/data/2013-01.json.gz'),
                      SelectionVariables(
                          ['date_heure', 'region', 'consommation_brute_totale', 'consommation_brute_gaz_totale']),
                      MoyenneGlissante(['consommation_brute_totale'], 10),
                      Normalisation(['consommation_brute_gaz_totale']),
                      ExportCsv('test6')])
tab2 = tab2_json.appliquer()
# print(tab2)

# Test 7 : Boxplot
reg = Pipeline([ImportCsv('./donnees/data/postesSynopAvecRegions.csv')])
reg = reg.appliquer()
tab1_csv = Pipeline([ImportCsv('./donnees/data/synop.201301.csv.gz'),
                     Jointure(reg, ['ID', 'numer_sta']),
                     SelectionVariables(
                         ['ID', 'Nom', 'Region', 't', 'rafper']),
                     Plot('t', 'rafper'),
                     Boxplot(['t', 'rafper'])])
tab1 = tab1_csv.appliquer()
