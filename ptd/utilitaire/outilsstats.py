class OutilsStats:
    """ Classe regroupant les outils de base de statistique

    Elle regroupe ainsi des méthodes de base telles que la somme, la moyenne, l'écart-type, la sélection de colonnes, 
    le repérage de l'indice d'un élément dans une liste, la suppression d'éléments en fonction de leur valeur ou encore 
    la conversion en un nombre flottant. 

    Attributes
    ----------
    - data : list 
        Liste sur laquelle une ou plusieurs opérations de base en statistique sont à effectuer 
        ou Nom des colonnes d'un jeu de données 

    Parameters
    ----------
    - data : list 
        Liste sur laquelle une ou plusieurs opérations de base en statistique sont à effectuer 
        ou Nom des colonnes d'un jeu de données 
    """

    def __init__(self, data):
        """ Constructeur 

        Parameters
        ----------
        data : list 
            Liste sur laquelle une ou plusieurs opérations de base en statistique sont à effectuer 
            ou Nom des colonnes d'un jeu de données 
        """
        self.__data = data

    def data(self):
        """ Renvoie la valeur de l'objet 

        Returns
        -------
        list 
            Valeur de l'objet 

        Examples
        --------
        >>> a = OutilsStats(['1', '2', '3'])
        >>> a.data()
        ['1', '2', '3']
        """
        return self.__data

    def somme(self):
        """ Calcule la somme des éléments d'une liste 

        Returns
        -------
        float
            La somme des éléments de la liste

        Examples
        --------
        >>> a = OutilsStats(['1', 2, '4', 'mq'])
        >>> a.somme()
        7.0
        >>> a = OutilsStats([])
        >>> a.somme()
        0
        >>> a = OutilsStats(['Temperature', 'Consommation gaz', 'Date'])
        >>> a.somme()
        0
        """
        self.nettoyage()  # On convertit les valeurs en nombre flottant et on supprime les éléments pour lequel la conversion est impossible
        somme = 0
        for i in range(len(self.__data)):
            somme += self.__data[i]
        return somme

    def moyenne(self):
        """ Calcule la moyenne des éléments d'une liste 

        Returns
        -------
        float
            La moyenne des éléments d'une liste 

        Examples
        --------
        >>> a = OutilsStats(['1', 2, '4', 'mq'])
        >>> a.moyenne()
        2.3333333333333335
        >>> a = OutilsStats([])
        >>> a.moyenne()
        0
        >>> a = OutilsStats(['Temperature', 'Consommation gaz', 'Date'])
        >>> a.moyenne()
        0
        """
        self.nettoyage()  # On convertit les valeurs en nombre flottant et on supprime les éléments pour lequel la conversion est impossible
        return self.somme()/len(self.__data) if len(self.__data) else 0

    def ecarttype(self):
        """ Calcule l'écart type des éléments d'une liste 

        Returns
        -------
        float
            L'écart-type des éléments d'une liste 

        Examples
        --------
        >>> a = OutilsStats(['1', 2, '4', 'mq'])
        >>> a.ecarttype()
        1.247219128924647
        >>> a = OutilsStats([])
        >>> a.ecarttype()
        0
        >>> a = OutilsStats(['Temperature', 'Consommation gaz', 'Date'])
        >>> a.ecarttype()
        0
        """
        self.nettoyage()  # On convertit les valeurs en nombre flottant et on supprime les éléments pour lequel la conversion est impossible
        var = 0
        moy = self.moyenne()
        for i in range(len(self.__data)):
            var += ((self.__data[i]) - moy)**2
        return pow(var / len(self.__data), 1/2) if len(self.__data) else 0

    def numeroCol(self, name):
        """ Fonction qui renvoie l'indice d'un élément dans une liste 

        Parameters 
        ----------
        name : str  
            Un élément d'une liste dont on recherche l'indice dans celle-ci

        Returns
        -------
        int
            L'indice d'un élément dans la liste 

        Examples
        --------
        >>> a = OutilsStats(['Temperature', 'Consommation gaz', 'Date'])
        >>> a.numeroCol('Date')
        2
        >>> a.numeroCol('Autre')
        """
        return self.__data.index(name) if name in self.__data else None

    def selectionCol(self, body, name):
        """ Renvoie une liste représentant la colonne sélectionnée dans une liste de listes en fonction du nom de la colonne 

        Parameters 
        ----------
        body : list de list 
            Jeu de données regroupant toutes les colonnes et dont il faut extraire l'une d'entre elles 
        name : str 
            Nom de la colonne à selectionner 

        Returns
        -------
        list 
            La colonne sélectionée 

        Examples
        --------
        >>> a = OutilsStats(['Temperature', 'Consommation gaz', 'Date'])
        >>> a.selectionCol([['1', 2, '4'], ['3', 6, '6']], 'Date')
        ['4', '6']
        >>> a.selectionCol([['1', 2, '4'], ['3', 6, '6']], 'Autre')
        []
        """
        liste = []
        num_col = self.numeroCol(name)
        for i in range(len(body)):
            if num_col is not None:
                liste.append(body[i][num_col])
        return liste

    def supprimerNA(self, name='mq'):
        """ Supprime un élément dans une liste 

        Parameters 
        ----------
        name : str, default 'mq'
            Nom de l'élément à supprimer dans la liste 

        Examples
        --------
        >>> a = OutilsStats(['1', 2, '4', 'mq'])
        >>> a.supprimerNA('mq')
        >>> a.data()
        ['1', 2, '4']
        >>> a.supprimerNA('3')
        >>> a.data()
        ['1', 2, '4']
        """
        if name in self.__data:
            self.__data.remove(name)

    def convertirFloat(self):
        """ Convertit les éléments d'une liste en nombre flottant lorsque c'est possible

        Examples
        --------
        >>> a = OutilsStats(['1', 2, '4', 'mq'])
        >>> a.convertirFloat()
        >>> a.data()
        [1.0, 2.0, 4.0, 'mq']
        >>> a = OutilsStats([1, 2, 4])
        >>> a.convertirFloat()
        >>> a.data()
        [1.0, 2.0, 4.0]
        """
        for i in range(len(self.__data)):
            try:
                self.__data[i] = float(self.__data[i])
            except:  # Si la conversion en nombre flottant est impossible, on garde la valeur d'origine
                pass

    def nettoyage(self):
        """ Nettoie une liste de données 

        La méthode permet de convertir les éléments de la liste en nombre flottant lorsque cela est possible. 
        Puis elle supprime les valeurs manquantes ('mq' ou ' ') ou bien toutes les valeurs de chaines de 
        caractères qui ne peuvent pas subir des opérations de somme, de moyenne ou d'écart-type.

        Examples
        --------
        >>> a = OutilsStats(['1', 2, '4', 'mq'])
        >>> a.nettoyage()
        >>> a.data()
        [1.0, 2.0, 4.0]
        """
        self.convertirFloat()
        i = 0
        while i < len(self.__data):
            # Si la valeur de l'élément est une chaine de caractères malgré la conversion en nombre flottant, on la supprime
            if type(self.__data[i]) == str:
                self.supprimerNA(self.__data[i])
            else:
                i += 1


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
